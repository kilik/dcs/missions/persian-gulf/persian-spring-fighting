# 51ème Escadron Griffon

* Simulator: DCS
* Version: 2.5.6+
* Mission: Persian Spring Fighting
* Map: Persian Gulf

## How to work on this mission ?

requirements (to develop on this mission): 

* DCS World 2.5+
* 7za.exe in your path [get 7zip Extra here](https://www.7-zip.org/download.html)
* git
* an IDE (notepad++, visual studio code...)
* npm - for that install [node.js](https://nodejs.org/en/download/)

Note: *Always edit mission with noon meteo*
 